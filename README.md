A traditional Russian card game. The game was written as one of the projects for the Python class in college. This particular game is for one player and is played against the computer, as well as a shortened version of the classic Durak.
In contrast to the standard rules, the presented version of the game does not provide more than one move by each player per round, i.e. the attacking player can play only one card. When playing under the standard rules, the attacker can flip cards of the same value that have already been played in the round, as long as the attacker has such cards and as long as the defender fights back or draws all the cards of the round.
Also, the presented version implements the following principle of card selection when defending and attacking by the computer: the computer chooses the card with the smallest face value. This principle is just one of many strategies possible in this game.

The complete rules for the game itself are provided below for your reference.

                                                “Durak” Rules
The deck of 36 cards must be shuffled before the game begins. Each player is dealt 6 cards face down. Then, one random card out of the deck is picked to be a trump (suit) card of the game (except the Ace card) and put face up under the deck (cards in the deck face down). A trump card of any rank beats all cards in the other three suits (for example, a 6 of trump beats an Ace of any other suit). The seniority of cards in a deck (from lower to higher value): 6, 7, 8, 9, 10, Jack (J), Queen (Q), King (K), Ace (A). The trump Ace in the game can’t be beaten, only being taken. Players hide their cards from other players during the game, however, the total number of cards in a player's hand must be seen.

The first move (a.k.a. attack) of the game belongs to the player who has the junior trump card in his hand (there is no obligation to play that junior trump card as the first card).

The player to the attacker's left is always the defender. After each round of attack, the play proceeds clockwise. If the attack succeeds, the defender loses their move, and the right of the attack passes to the player on the defender's left. If the attack fails, the defender becomes the next attacker.

Attack:

The attacker moves by playing a card face-up on the table as an attacking card. After the first attack, if the defender is successful, the attacker may launch a new attack of the round. Any new attack in the round can only be made if the new attack card matches the value of any card which has already been played during that round. If the attacker cannot make a new attack or pass, then the player to the left of the defender may start a new attack of the round or pass the chance to attack the next player, going clockwise around the table. For each new attack of the round that is defended successfully by the defender, the player who led that attack may start a new attack or pass and so ongoing clockwise around the table. In each round, there cannot be more attacks than the defender has cards on hand before the round starts.

Defense:

To beat a card, you need to cover the attacking card with the senior card of the same suit or a trump card. If the attacking card is a trump card, only the senior trump card can beat it. The defending card is placed on top of the attacking card, overlapping it, so both cards are visible, and it is clear which cards are attacking and defending cards. If the defensive player is unwilling or unable to beat at least one card, he must take all cards of the round and skip his moving (attacking) turn.

After a round of successful defense - all cards from the round are discarded so that they no longer take part in the game (no players may examine the discard pile at any point). All players refill their hands with up to 6 cards after around. The first player to refill is the player who attacked in the round, then other players who attacked in the round clockwise, and the defender refills last. Players refill cards until the whole deck is gone, including the opened trump card. 

After a round of an unsuccessful defense - a defense player skips the refilling of his hand.
After the deck is empty, the game continues with the remaining cards in hand until all players, except one, get rid of all their cards. That player remaining with cards is a loser (the “fool”, which actually the word “durak” means).
