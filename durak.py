#Alex Suvorov: card game "Durak" (shortened version)
import random

class Card:
    def __init__(self, suit, face):
        self.suit = suit
        self.face = face
        self.trump = False
    def value(self):
        if type(self.face) == int:
            return self.face
        else:
            valueDict = {"J":11, "Q":12, "K":13, "A":14}
            value = valueDict[self.face]
            return value
    def __str__(self):
        return "{} of {}s".format(self.face, self.suit)

class Cards:
    def __str__(self):
        toPrint = []
        x = 1
        for card in self.cards:
            y = "["+str(x)+"]"+" "+str(card)
            toPrint.append(y)
            x+=1
        return "\n ".join(toPrint)

class Deck(Cards):
    def __init__(self, trump):
        self.cards = []
        suits = ["Club", "Diamond", "Heart", "Spade"]
        faces = [6, 7, 8, 9, 10, "J", "Q", "K", "A"]
        for suit in suits:
            for face in faces:
                newCard = Card(suit, face)
                if newCard.suit == trump:
                    newCard.trump = True
                self.cards.append(newCard)
    def shuffle(self):
        random.shuffle(self.cards)
    def deal(self):
        return self.cards.pop()
    def updated_deck(self):
        return len(self.cards)

class Hand(Cards):
    def __init__(self,deck):
        self.cards = []
        self.turn = False
        for i in range(6):
            self.hit(deck)
    def hit(self, deck):
        if deck.cards and len(self.cards)<6:
            dealtCard = deck.deal()
            self.cards.append(dealtCard)

def print_hit_statement(deck):
        if deck.cards:
            PC.hit(deck)
            print("PC hits the card.")
            player.hit(deck)
            print("player hits the card.")

#making dictionary for picking card by number from the hand.
def arrange_cards():
    choosing_card_by_number = {}
    x = 1
    y = 0
    for i in range(36):
        choosing_card_by_number[str(x)] = y
        x += 1
        y += 1
    return choosing_card_by_number

#visible numeration of cards in hand for user usability during the attack.
def arrange_cards_attack(hand):
    ok_go = False
    numb_lst = ["1", "2","3","4","5","6","7","8","9","10","11","12","13","14","15"]
    while ok_go == False:
        choice = input("Choose a card for the attack: ")
        choosing_card_by_number = arrange_cards()
        if choice in numb_lst:
            if choosing_card_by_number.get(choice)<=len(hand.cards):
                for key in choosing_card_by_number.keys():
                    if choice == key:
                        return hand.cards[choosing_card_by_number.get(choice)]
                        ok_go = True
        else:
            print("Wrong input! Please choose cards from the hand by its number.")

#visible numeration of cards in hand for user usability during the defense.
def arrange_cards_defense(hand):
    ok_go = False
    numb_lst = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"]
    while ok_go == False:
        choice = input("Choose a card for the defense: ")
        choosing_card_by_number = arrange_cards()
        if choice in numb_lst:
            if choosing_card_by_number.get(choice)<=len(hand.cards):
                for key in choosing_card_by_number.keys():
                    if choice == key:
                        return hand.cards[choosing_card_by_number.get(choice)]
                        ok_go = True
        #"t" is for take here, when player decide to take card, since couldn't defend in the round.
        elif choice == "t":
            return "t"
        else:
            print("Wrong input! Please choose cards from the hand by its number.")

#this function is using the game rules to find the first attacking player of the game.
def first_attacker_generator(player, PC):
    player_trump_cards = []
    pc_trump_cards = []
    for card in player.cards:
        if card.trump == True:
            player_trump_cards.append(card.value())
    for card in PC.cards:
        if card.trump == True:
            pc_trump_cards.append(card.value())
    print(f"player's trump values {player_trump_cards}.")
    print(f"PC's trump values {pc_trump_cards}.")
    if not player_trump_cards and pc_trump_cards:
        print("PC first attack!")
        return PC
    elif player_trump_cards and not pc_trump_cards:
        print("player first attack!")
        return player
    #when both hands (player's and PC's) don't have trump cards.
    elif not player_trump_cards and not pc_trump_cards:
        coin = random.randint(0, 1)
        if coin == 0:
            print("player first attack!")
            return player
        else:
            print("PC first attack!")
            return PC
    #when both hands have trump card(s) -> the hand with the junior value trump card has the attacking priority.
    elif player_trump_cards and pc_trump_cards:
        player_min = min(player_trump_cards)
        pc_min = min(pc_trump_cards)
        if player_min < pc_min:
            print("player first attack!")
            return player
        else:
            print("PC first attack!")
            return PC

#PC attacking strategy.
def PC_attack(hand):
    if hand.cards:
        cards_of_choice = []
        for card in hand.cards:
            if card.trump != True:
                cards_of_choice.append(card.value())
        # for the case when only trump cards left -> to attack with the junior value one.
        if not cards_of_choice:
            pc_trump_cards = []
            for card in hand.cards:
                pc_trump_cards.append(card.value())
            pc_min = min(pc_trump_cards)
            for card in hand.cards:
                if card.value() == pc_min:
                    return card
        else:
            min_value_card = min(cards_of_choice)
            for card in hand.cards:
                if card.value() == min_value_card:
                    return card

#PC defending strategy.
def PC_defense(pcHand, playerAttackCard):
    for card in pcHand.cards:
        if playerAttackCard.trump == True and card.suit == playerAttackCard.suit and card.value() > playerAttackCard.value():
            print(f"Player attacked with ['{playerAttackCard}']. PC defended with trump card ['{card}'].")
            return card

    for card in pcHand.cards:
        if playerAttackCard.trump == True and card.suit == playerAttackCard.suit and card.value() < playerAttackCard.value():
            print("PC takes the card!")
            return playerAttackCard

    for card in pcHand.cards:
        if playerAttackCard.trump == True and card.suit != playerAttackCard.suit:
            print("PC takes the card!")
            return playerAttackCard

    for card in pcHand.cards:
        if playerAttackCard.trump != True and card.suit == playerAttackCard.suit and card.value() > playerAttackCard.value():
            print(f"Player attacked with ['{playerAttackCard}']. PC defended with ['{card}'].")
            return card

    for card in pcHand.cards:
        if playerAttackCard.trump != True and card.suit != playerAttackCard.suit and card.trump == True:
            print(f"Player attacked with ['{playerAttackCard}']. PC defended with trump card ['{card}'].")
            return card

    for card in pcHand.cards:
        if playerAttackCard.trump != True and card.suit == playerAttackCard.suit and card.value() < playerAttackCard.value():
            print("PC takes the card!")
            return playerAttackCard

    for card in pcHand.cards:
        if playerAttackCard.trump != True and card.suit != playerAttackCard.suit:
            print("PC takes the card!")
            return playerAttackCard


#update of the quantity left on the deck and printing current hands
def refresh_on_deck_on_hands(deck, player, PC):
    print("In the deck is left: " + str(deck.updated_deck()))
    if player.cards and PC.cards:
        print("The Trump Card of the Game: " + trump)
        print("*Player's Hand:", "\n", player)
        print("*PC's Hand (should be hidden):", "\n", PC)
    elif not deck.cards and not player.cards:
            print("player WINS!")
            #gameOver = True
            return True
    elif not deck.cards and not PC.cards:
            print("PC WINS!")
            #gameOver = True
            return True

# Start of game
###############
print("************************************\n Hello to the Durak Game!\n To attack/defense choose the card number,\n to take in defense press 't'.\n************************************")
discarded_pile = []
round_pile = []
suits = ["Club", "Diamond", "Heart", "Spade"]
trump = random.choice(suits)
myDeck = Deck(trump)
myDeck.shuffle()
player = Hand(myDeck)
PC = Hand(myDeck)
refresh_on_deck_on_hands(myDeck, player, PC)

gameOver = False
attacker = first_attacker_generator(player, PC)
while gameOver is False:
    if attacker == player:
        attacking_card_choice = arrange_cards_attack(player)
        print(f"Card(s) in the round: ['{attacking_card_choice}'].\n************************************")
        defensing_card_choice = PC_defense(PC, attacking_card_choice)
        #the next indented if statement block tells only that PC took the card ->
        # -> (PC_defence function from above returned the attacking card, as the card that was taken by the PC):
        if defensing_card_choice == attacking_card_choice:
            player.cards.remove(attacking_card_choice)
            PC.cards.append(attacking_card_choice)
            print(f"Card(s) in the discarded pile {discarded_pile}.")
            player.hit(myDeck)
            print("player hits the card.")
            i = refresh_on_deck_on_hands(myDeck, player, PC)
            if i == True:
                break
        #here PC defended successfully and both hands are get rid of round cards and hit the deck:
        elif defensing_card_choice != attacking_card_choice:
            print(f"Card(s) in the round: ['{attacking_card_choice}', '{defensing_card_choice}'].")
            print("Cards of the round are discarded.")
            player.cards.remove(attacking_card_choice)
            PC.cards.remove(defensing_card_choice)
            discarded_pile.append(str(attacking_card_choice))
            discarded_pile.append(str(defensing_card_choice))
            print(f"Card(s) in the discarded pile {discarded_pile}.")
            print_hit_statement(myDeck)
            i = refresh_on_deck_on_hands(myDeck, player, PC)
            if i == True:
                break
            attacker = PC

    elif attacker == PC:
        attacking_card_choice = PC_attack(PC)
        print(f"PC attacks with ['{attacking_card_choice}'].")
        print(f"Card(s) in the round: ['{attacking_card_choice}'].\n************************************")
        defensing_card_choice = arrange_cards_defense(player)
        #when player decided to take cards of the round:
        if defensing_card_choice == "t":
            print("player took the card!")
            PC.cards.remove(attacking_card_choice)
            player.cards.append(attacking_card_choice)
            if myDeck.cards:
               PC.hit(myDeck)
               print("PC hits the card.")
            i = refresh_on_deck_on_hands(myDeck, player, PC)
            if i == True:
                break
        #when player defended successfully with senior card against same suit attacking card:
        elif defensing_card_choice.suit == attacking_card_choice.suit and defensing_card_choice.value() > attacking_card_choice.value():
            print(f"PC attacked with ['{attacking_card_choice}']. Player defended with ['{defensing_card_choice}']. Cards of the round are discarded.")
            discarded_pile.append(str(attacking_card_choice))
            discarded_pile.append(str(defensing_card_choice))
            print(f"Card(s) in the discarded pile {discarded_pile}.")
            player.cards.remove(defensing_card_choice)
            PC.cards.remove(attacking_card_choice)
            print_hit_statement(myDeck)
            i = refresh_on_deck_on_hands(myDeck, player, PC)
            if i == True:
                break
            attacker = player
        # when player defended successfully with trump card against not trump attacking card:
        elif defensing_card_choice.trump == True and attacking_card_choice.trump == False:
            print(f"PC attacked with ['{attacking_card_choice}']. Player defended with ['{defensing_card_choice}']. Defender used the trump card. Cards of the round are discarded.")
            discarded_pile.append(str(attacking_card_choice))
            discarded_pile.append(str(defensing_card_choice))
            print(f"Card(s) in the discarded pile {discarded_pile}.")
            player.cards.remove(defensing_card_choice)
            PC.cards.remove(attacking_card_choice)
            print_hit_statement(myDeck)
            i = refresh_on_deck_on_hands(myDeck, player, PC)
            if i == True:
                break
            attacker = player